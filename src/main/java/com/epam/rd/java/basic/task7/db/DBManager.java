package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance==null)
			instance = new DBManager();
		return instance;
	}

	private Connection connection;
	private DBManager()  {

		try{
			String url="";
			//Class.forName("com.mysql.jdbc.Driver");
			Class.forName("com.mysql.cj.jdbc.Driver");

			Properties prop = new Properties();
			InputStream input;

			try {
				input = new FileInputStream("app.properties");
				prop.load(input);
				url = prop.getProperty("connection.url");
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		this.connection= DriverManager.getConnection(url);

		}
		catch (Exception e){

			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {

		List<User> list = new ArrayList<>();
		try {

			Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery("select * from users");

			while(resultSet.next()){
				User u = new User();
				u.setLogin(resultSet.getString("login"));
				u.setId(resultSet.getInt("id"));
				list.add(u);
			}
			resultSet.close();

		}catch (SQLException e){

			throw new DBException("findAllUsers",e);
		}
		return  list;
	}

	public boolean insertUser(User user) throws DBException {
		try {

			//String query = "insert into users values(default,'" +user.getLogin()+"')";
			String query = "insert into users values(default,?)";
			PreparedStatement stm = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			stm.setString(1,user.getLogin());
			stm.executeUpdate();
			ResultSet r = stm.getGeneratedKeys();
			r.next();
			user.setId(r.getInt(1));
			r.close();
			return true;

		}catch (SQLException e){

			throw new DBException("insertUser",e);
		}

	}

	public boolean deleteUsers(User... users) throws DBException {

		try {

			String ids = Arrays.stream(users).map(User::getId).map(Object::toString).collect(Collectors.joining(","));
			Statement stmt = connection.createStatement();
			return stmt.execute("delete from users where id in (" + ids + ")");

		}catch (SQLException e){

			throw new DBException("deleteUsers",e);
		}

	}

	public User getUser(String login) throws DBException {
		try {

			Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery("select * from users where login ='"+ login+"'");

			User u = new User();
			while(resultSet.next()){
				u.setLogin(resultSet.getString("login"));
				u.setId(resultSet.getInt("id"));
			}
			resultSet.close();
			return u;

		}catch (SQLException e){

			throw new DBException("getUser",e);
		}
	}

	public Team getTeam(String name) throws DBException {
		try {

			Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery("select * from teams where name ='"+ name+"'");

			Team u = new Team();
			while(resultSet.next()){
				u.setName(resultSet.getString("name"));
				u.setId(resultSet.getInt("id"));
			}
			resultSet.close();
			return u;

		}catch (SQLException e){

			throw new DBException("getTeam",e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> list = new ArrayList<>();
		try {

			Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery("select * from teams");

			while(resultSet.next()){
				Team u = new Team();
				u.setName(resultSet.getString("name"));
				u.setId(resultSet.getInt("id"));
				list.add(u);
			}
			resultSet.close();

		}catch (SQLException e){

			throw new DBException("findAllTeams",e);
		}
		return  list;
	}

	public boolean insertTeam(Team team) throws DBException {

		try{
			String query = "insert into teams values(default,?)";
			PreparedStatement stm = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			stm.setString(1, team.getName());
			stm.executeUpdate();
			ResultSet r = stm.getGeneratedKeys();
			r.next();
			team.setId(r.getInt(1));
			r.close();
			return true;
		}
		catch (SQLException e){
			throw new DBException("insertTeam",e);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		try {
			connection.setAutoCommit(false);
			PreparedStatement stm = connection.prepareStatement("insert into users_teams(user_id,team_id) values (?,?)");
			stm.setInt(1,user.getId());
			for (Team t: teams) {
				stm.setInt(2,t.getId());
				stm.executeUpdate();
			}
			connection.commit();
			connection.setAutoCommit(true);
			return  true;
		}catch (SQLException e){

			try{
				connection.rollback();
				connection.setAutoCommit(true);
			}
			catch (SQLException ee){
				Throwable t = new Throwable(ee);
				t.initCause(e);
				throw new DBException("setTeamsForUser",t);
			}
			throw new DBException("setTeamsForUser",e);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {

		List<Team> list = new ArrayList<>();
		try {

			Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery(
					"select t.* from teams t inner join users_teams ut on ut.team_id = t.id where ut.user_id="
							+user.getId());

			while(resultSet.next()){
				Team u = new Team();
				u.setName(resultSet.getString("name"));
				u.setId(resultSet.getInt("id"));
				list.add(u);
			}
			resultSet.close();

		}catch (SQLException e){

			throw new DBException("findAllTeams",e);
		}
		return  list;


	}

	public boolean deleteTeam(Team team) throws DBException {
		try {

			Statement stmt = connection.createStatement();
			return stmt.execute("delete  from teams where name ='"+ team.getName()+"' and id= "+team.getId());

		}catch (SQLException e){

			throw new DBException("deleteTeam",e);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		try {

			Statement stmt = connection.createStatement();
			stmt.execute("update teams set name='"+team.getName()+"' where id= "+team.getId());

			return true;
		}catch (SQLException e){

			throw new DBException("updateTeam",e);
		}
	}

}
