package com.epam.rd.java.basic.task7.db;

import java.sql.SQLException;

public class DBException extends SQLException {

	private String message;
	private Throwable cause;
	public DBException(String message, Throwable cause) {

		this.message = message;
		this.cause = cause;
		cause.printStackTrace();
	}

}
